import frappe

def execute():
	for expense in frappe.get_all("Expense", filters={"docstatus": 2}):
		frappe.db.set_value("Expense", expense.name, "status", "Cancelled")