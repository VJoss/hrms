import frappe

from frappe.tests.utils import FrappeTestCase
from frappe.utils import getdate, get_last_day, month_diff, flt

from erpnext.setup.doctype.employee.test_employee import make_employee

from hrms.regional.france.hr.utils import daterange, allocate_earned_leaves
from hrms.regional.france.tests.utils import setup_tests_for_france
from hrms.hr.doctype.leave_ledger_entry.leave_ledger_entry import process_expired_allocation

PERIODS = [
	("2018-06-01", "2019-05-31"),
	("2019-06-01", "2020-05-31"),
	("2020-06-01", "2021-05-31"),
	("2021-06-01", "2022-05-31"),
	("2022-06-01", "2023-05-31"),
	("2023-06-01", "2024-05-31"),
	("2024-06-01", "2025-05-31")
]


class TestFranceLeavesCalculation(FrappeTestCase):
	@classmethod
	def setUpClass(cls):
		setup_tests_for_france()

	@classmethod
	def tearDownClass(cls):
		frappe.db.rollback()

	def setUp(self):
		self.employee = make_employee("testfranceleavesemp@example.com", company="_Test Company")
		frappe.db.set_value("Employee", self.employee, "holiday_list", f"_Test France Holiday List 2024")

		frappe.db.delete("Leave Ledger Entry")
		frappe.db.delete("Leave Allocation")
		frappe.db.delete("Leave Application")

	def test_conges_payes_sur_jours_ouvrables(self):
		contract = frappe.get_doc({
			"doctype": "Employment Contract",
			"employee": self.employee,
			"company": "_Test Company",
			"date_of_joining": frappe.db.get_value("Employee", self.employee, "date_of_joining"),
			"contract_type": "Temps plein",
			"designation": "Analyst",
			"monday": 7,
			"tuesday": 7,
			"wednesday": 7,
			"thursday": 7,
			"friday": 7
		})
		contract.append("leave_types", {
			"leave_type": "Congés Payés sur jours ouvrables"
		})
		contract.insert(ignore_if_duplicate=True)

		self.calculate_leaves("Congés Payés sur jours ouvrables", 2.5, 30)

	def test_conges_payes_sur_jours_ouvres(self):
		contract = frappe.get_doc({
			"doctype": "Employment Contract",
			"employee": self.employee,
			"company": "_Test Company",
			"date_of_joining": frappe.db.get_value("Employee", self.employee, "date_of_joining"),
			"contract_type": "Temps plein",
			"designation": "Analyst",
			"monday": 7,
			"tuesday": 7,
			"wednesday": 7,
			"thursday": 7,
			"friday": 7
		})
		contract.append("leave_types", {
			"leave_type": "Congés Payés sur jours ouvrés"
		})
		contract.insert(ignore_if_duplicate=True)

		self.calculate_leaves("Congés Payés sur jours ouvrés", 2.08, 25)


	def test_conges_payes_sur_jours_ouvrables_temps_partiel(self):
		contract = frappe.get_doc({
			"doctype": "Employment Contract",
			"employee": self.employee,
			"company": "_Test Company",
			"date_of_joining": frappe.db.get_value("Employee", self.employee, "date_of_joining"),
			"contract_type": "Temps plein",
			"designation": "Analyst",
			"monday": 7,
			"tuesday": 7,
			"wednesday": 0,
			"thursday": 0,
			"friday": 0
		})
		contract.append("leave_types", {
			"leave_type": "Congés Payés sur jours ouvrables"
		})
		contract.insert(ignore_if_duplicate=True)

		self.calculate_leaves("Congés Payés sur jours ouvrables", 2.5, 30)


	def test_conges_payes_sur_jours_ouvres_temps_partiel(self):
		contract = frappe.get_doc({
			"doctype": "Employment Contract",
			"employee": self.employee,
			"company": "_Test Company",
			"date_of_joining": frappe.db.get_value("Employee", self.employee, "date_of_joining"),
			"contract_type": "Temps plein",
			"designation": "Analyst",
			"monday": 7,
			"tuesday": 7,
			"wednesday": 0,
			"thursday": 0,
			"friday": 0
		}).insert(ignore_if_duplicate=True)

		contract.append("leave_types", {
			"leave_type": "Congés Payés sur jours ouvrés"
		})

		self.calculate_leaves("Congés Payés sur jours ouvrés", 2.08, 25)


	def calculate_leaves(self, leave_type, monthly, yearly, periods=None):
		current_year = None
		for period in periods or PERIODS:
			if current_year != getdate(period[0]).year:
				frappe.db.set_value("Employee", self.employee, "holiday_list", f"_Test France Holiday List {getdate(period[0]).year}")
				current_year = getdate(period[0]).year

			for date in daterange(getdate(period[0]), getdate(period[1])):
				frappe.flags.current_date = date
				allocate_earned_leaves(date)
				leave_allocations = get_leave_allocations_for_employee(date, self.employee, leave_type)
				if not leave_allocations:
					continue
				leave_allocation = frappe.get_doc("Leave Allocation", leave_allocations[0])
				if date == get_last_day(date):
					if date == getdate(period[1]):
						self.assertEqual(flt(leave_allocation.total_leaves_allocated - leave_allocation.unused_leaves, 2), yearly)
					else:
						self.assertEqual(flt(leave_allocation.total_leaves_allocated - leave_allocation.unused_leaves, 2), flt(monthly * month_diff(date, getdate(period[0])), 2))


	def test_leave_application_jours_ouvrables(self):
		emp = make_employee("testfranceleavesappro@example.com", company="_Test Company")
		approver = frappe.db.get_value("Employee", emp, "user_id")
		frappe.db.set_value("Employee", self.employee, "leave_approver", approver)

		contract = frappe.get_doc({
			"doctype": "Employment Contract",
			"employee": self.employee,
			"company": "_Test Company",
			"date_of_joining": frappe.db.get_value("Employee", self.employee, "date_of_joining"),
			"contract_type": "Temps plein",
			"designation": "Analyst",
			"monday": 7,
			"tuesday": 7,
			"wednesday": 0,
			"thursday": 0,
			"friday": 0
		})
		contract.append("leave_types", {
			"leave_type": "Congés Payés sur jours ouvrables"
		})
		contract.insert(ignore_if_duplicate=True)

		self.calculate_leaves("Congés Payés sur jours ouvrables", 2.5, 30, PERIODS[1:2])

		leaves = [
			("2019-08-02", "2019-08-12"),
			("2019-10-30", "2019-11-04"),
			("2019-12-31", "2020-01-06"),
			("2020-04-03", "2020-04-13"),
		]

		total_leaves = {
			0: 6,
			1: 2,
			2: 3,
			3: 6
		}
		for index, leave in enumerate(leaves):
			doc = frappe.get_doc({
				"doctype": "Leave Application",
				"employee": self.employee,
				"leave_type": "Congés Payés sur jours ouvrables",
				"from_date": leave[0],
				"to_date": leave[1],
				"leave_approver": frappe.db.get_value("Employee", approver, "user_id")
			}).insert()
			self.assertEqual(total_leaves.get(index), doc.total_leave_days)

	def test_leave_application_jours_ouvres(self):
		emp = make_employee("testfranceleavesappro@example.com", company="_Test Company")
		approver = frappe.db.get_value("Employee", emp, "user_id")
		frappe.db.set_value("Employee", self.employee, "leave_approver", approver)

		contract = frappe.get_doc({
			"doctype": "Employment Contract",
			"employee": self.employee,
			"company": "_Test Company",
			"date_of_joining": frappe.db.get_value("Employee", self.employee, "date_of_joining"),
			"contract_type": "Temps plein",
			"designation": "Analyst",
			"monday": 7,
			"tuesday": 7,
			"wednesday": 0,
			"thursday": 0,
			"friday": 0
		})
		contract.append("leave_types", {
			"leave_type": "Congés Payés sur jours ouvrés"
		})
		contract.insert(ignore_if_duplicate=True)


		self.calculate_leaves("Congés Payés sur jours ouvrés", 2.08, 25, PERIODS[1:2])

		leaves = [
			("2019-08-02", "2019-08-12"),
			("2019-10-30", "2019-11-04"),
			("2019-12-31", "2020-01-06"),
			("2020-04-03", "2020-04-13"),
		]

		total_leaves = {
			0: 5,
			1: 1,
			2: 2,
			3: 5
		}
		for index, leave in enumerate(leaves):
			doc = frappe.get_doc({
				"doctype": "Leave Application",
				"employee": self.employee,
				"leave_type": "Congés Payés sur jours ouvres",
				"from_date": leave[0],
				"to_date": leave[1],
				"leave_approver": frappe.db.get_value("Employee", approver, "user_id")
			}).insert()
			self.assertEqual(total_leaves.get(index), doc.total_leave_days)


	def test_carry_forward(self):
		emp = make_employee("testfranceleavesappro@example.com", company="_Test Company")
		approver = frappe.db.get_value("Employee", emp, "user_id")
		frappe.db.set_value("Employee", self.employee, "leave_approver", approver)
		leave_type = "Congés Payés sur jours ouvrables"

		contract = frappe.get_doc({
			"doctype": "Employment Contract",
			"employee": self.employee,
			"company": "_Test Company",
			"date_of_joining": frappe.db.get_value("Employee", self.employee, "date_of_joining"),
			"contract_type": "Temps plein",
			"designation": "Analyst",
			"monday": 7,
			"tuesday": 7,
			"wednesday": 0,
			"thursday": 0,
			"friday": 0
		})
		contract.append("leave_types", {
			"leave_type": leave_type
		})
		contract.insert(ignore_if_duplicate=True)

		leaves = {
			2018: [
				("2018-06-08", "2018-06-18"),
				("2018-08-03", "2018-08-20"),
				("2018-12-31", "2019-01-07"),
			],
			2019: [
				("2019-08-02", "2019-08-12"),
				("2019-10-30", "2019-11-04"),
				("2019-12-31", "2020-01-06"),
				("2020-04-03", "2020-04-13"),
			],
			2020: []
		}

		unused_leaves = {
			2018: 0,
			2019: 9,
			2020: 22,
		}

		current_year = None
		for period in PERIODS[0:3]:
			if current_year != getdate(period[0]).year:
				frappe.db.set_value("Employee", self.employee, "holiday_list", f"_Test France Holiday List {getdate(period[0]).year}")
				current_year = getdate(period[0]).year

			for leave in leaves[current_year]:
				doc = frappe.get_doc({
					"doctype": "Leave Application",
					"employee": self.employee,
					"leave_type": leave_type,
					"from_date": leave[0],
					"to_date": leave[1],
					"leave_approver": frappe.db.get_value("Employee", approver, "user_id")
				}).insert()
				doc.status = "Approved"
				doc.submit()


			self.calculate_leaves(leave_type, 2.5, 30, [period])

			for date in daterange(getdate(period[0]), getdate(period[1])):
				allocate_earned_leaves(date)
				leave_allocations = get_leave_allocations_for_employee(date, self.employee, leave_type)
				if not leave_allocations:
					continue
				leave_allocation = frappe.get_doc("Leave Allocation", leave_allocations[0])

				process_expired_allocation()

			self.assertEqual(leave_allocation.unused_leaves, unused_leaves[current_year])


	def test_calculate_non_earned_leaves_in_business_days(self):
		leave_type = frappe.get_doc({
			"doctype": "Leave Type",
			"leave_type_name": "Mariage - Non cadres",
			"name": "Mariage - enfant",
			"max_leaves_allowed": 4,
			"calculate_in_business_days": 1
		}).insert(ignore_if_duplicate=True)

		contract = frappe.get_doc({
			"doctype": "Employment Contract",
			"employee": self.employee,
			"company": "_Test Company",
			"date_of_joining": frappe.db.get_value("Employee", self.employee, "date_of_joining"),
			"contract_type": "Temps plein",
			"designation": "Associate",
			"monday": 7,
			"tuesday": 7,
			"wednesday": 7,
			"thursday": 7,
			"friday": 7
		})
		contract.append("leave_types", {
			"leave_type": leave_type.name
		})
		contract.insert(ignore_if_duplicate=True)
		contract.run_method("update_leaves")


		leave_application = frappe.new_doc("Leave Application")
		leave_application.employee = self.employee
		leave_application.leave_type = leave_type.name
		leave_application.from_date = "2024-11-27" # TODO: Make future proof tests
		leave_application.to_date = "2024-12-03"
		leave_application.insert()
		self.assertEqual(leave_application.total_leave_days, 4)


	def test_calculate_non_earned_leaves_in_working_days(self):
		leave_type = frappe.get_doc({
			"doctype": "Leave Type",
			"leave_type_name": "Mariage - Cadres",
			"name": "Mariage - enfant",
			"max_leaves_allowed": 4,
			"calculate_in_business_days": 0
		}).insert(ignore_if_duplicate=True)

		contract = frappe.get_doc({
			"doctype": "Employment Contract",
			"employee": self.employee,
			"company": "_Test Company",
			"date_of_joining": frappe.db.get_value("Employee", self.employee, "date_of_joining"),
			"contract_type": "Temps plein",
			"designation": "Associate",
			"monday": 7,
			"tuesday": 7,
			"wednesday": 7,
			"thursday": 7,
			"friday": 7
		})
		contract.append("leave_types", {
			"leave_type": leave_type.name
		})
		contract.insert(ignore_if_duplicate=True)
		contract.run_method("update_leaves")


		leave_application = frappe.new_doc("Leave Application")
		leave_application.employee = self.employee
		leave_application.leave_type = leave_type.name
		leave_application.from_date = "2024-11-27" # TODO: Make future proof tests
		leave_application.to_date = "2024-12-03"
		leave_application.insert()
		self.assertEqual(leave_application.total_leave_days, 3)


def get_leave_allocations_for_employee(date, employee, leave_type):
	return frappe.get_all(
		"Leave Allocation",
		filters={
			"from_date": ("<=", date),
			"to_date": (">=", date),
			"employee": employee,
			"docstatus": 1,
			"leave_type": leave_type
		},
		fields=["name", "employee", "from_date", "to_date", "leave_policy_assignment",
			"leave_policy", "company", "leave_type", "new_leaves_allocated",
			"total_leaves_allocated"
		]
	)