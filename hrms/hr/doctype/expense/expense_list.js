frappe.listview_settings["Expense"] = {
	onload: function (doclist) {
		const action = () => {
			const selected_docs = doclist.get_checked_items();
			const docnames = doclist.get_checked_items(true);

			if (selected_docs.length > 0) {
				for (let doc of selected_docs) {
					if (doc.docstatus != 1) {
						frappe.throw(__("Cannot create an expense claim from Draft or Cancelled documents."));
					}
				}

				frappe.new_doc("Expense Claim")
					.then(() => {
						// Empty out the child table before inserting new ones
						cur_frm.set_value("expenses", []);

						// We don't want to use `map_current_doc` since it brings up
						// the dialog to select more items. We just want the mapper
						// function to be called.
						frappe.call({
							type: "POST",
							method: "frappe.model.mapper.map_docs",
							args: {
								"method": "hrms.hr.doctype.expense.expense.make_expense_claim",
								"source_names": docnames,
								"target_doc": cur_frm.doc
							},
							callback: function (r) {
								if (!r.exc) {
									frappe.model.sync(r.message);
									cur_frm.dirty();
									cur_frm.refresh();
								}
							}
						});
					})
			}
		};

		doclist.page.add_action_item(__("Make an expense claim"), action, false);
	}
}