from frappe import _


def get_data():
	return {
		"fieldname": "reference_name",
		"internal_links": {"Employee Advance": ["advances", "employee_advance"], "Bank Transaction": ["payment_entries", "payment_entry"]},
		"transactions": [
			{"label": _("Payment"), "items": ["Payment Entry", "Journal Entry"]},
			{"label": _("Reference"), "items": ["Employee Advance"]},
			{"label": _("Bank Transactions"), "items": ["Bank Transaction"]},
		],
	}
