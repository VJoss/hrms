// Copyright (c) 2018, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt
frappe.views.calendar["Attendance"] = {
	field_map: {
		start: "start",
		end: "end",
		id: "name",
		title: "title",
		allDay: "all_day",
		color: "color",
		secondary_status: "status",
	},
	get_css_class: function (data) {
		if (data.doctype === "Holiday") return "default";
		else if (data.doctype === "Attendance") {
			if (data.status === "Absent" || data.status === "On Leave") {
				return "danger";
			}
			if (data.status === "Half Day") return "warning";
			return "success";
		}
	},
	secondary_status_color: {
		"Present": "green",
		"Work From Home": "red",
		"Absent": "red",
		"On Leave": "red",
		"Half Day": "orange"
	},
	options: {
		header: {
			left: "prev,next today",
			center: "title",
			right: "month",
		},
		displayEventTime: false
	},
	get_events_method: "hrms.hr.doctype.attendance.attendance.get_events",
	default_resource: "employee",
	excluded_resources: ["naming_series", "status", "shift", "leave_type", "leave_application", "attendance_request"]
};
